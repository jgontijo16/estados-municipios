package dev.gontijo.resource;

import java.net.URI;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dev.gontijo.model.Estado;

@Path("/estados")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EstadoResource {
	
	@GET
	public List<Estado> list(){
		return Estado.listAll();
	}
	
	@POST
	@Transactional
	public Response create(Estado estado){
		estado.persist();
		return Response.created(URI.create("/estados/" + estado.id)).build();
	}
	
	@PUT
	@Path("/{id}")
	@Transactional
	public Estado update(@PathParam("id") Long id, Estado estado) {
		Estado entidade = Estado.findById(id);
		if(entidade == null) {
			throw new NotFoundException();
		}
		entidade.name = estado.name;
		return entidade;
	}
	
	@DELETE
	@Path("/{id}")
	@Transactional
	public void delete(@PathParam("id") Long id) {
		Estado entidade = Estado.findById(id);
		if(entidade == null) {
			throw new NotFoundException();
		}
		entidade.delete();
	}
	
}
