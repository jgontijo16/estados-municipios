package dev.gontijo.model;

import javax.persistence.Entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
public class Estado extends PanacheEntity{
	public String name;
	
	public Estado() {
		// TODO Auto-generated constructor stub
	}

	public Estado(String name) {
		this.name = name;
	}
	
}
